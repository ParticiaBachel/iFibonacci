This program is a collection of two functions. Function explaination is given below:

Fibonacci series is used in series of application in different fields of science. Basic idea is the next numbers are a function of previous numbers in the series.

1. fib1()
    Arguments need = 1
        arg_1 = N = number of fibonacci series elements need
    return variable = fibonacci series with number of elements N

2. fib2()
    Arguments need = 1
        arg_1 = N is an integer = number of elements needed to be generated in fibonacci series
    return variable = N elements with each one of them corresponds to fibonacci sequence.

To know more about this program contact me <a href="http://99dollardesigners.com/">here</a>.